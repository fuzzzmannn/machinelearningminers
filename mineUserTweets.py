import tweepy #https://github.com/tweepy/tweepy
import miningUtils
import json
import time

def getUserTweets(screen_name, tweet_count):
	consumer_key, consumer_secret, access_key, access_secret = miningUtils.loadKeys("../../keys/keys.txt")

	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_key, access_secret)
	api = tweepy.API(auth)
	jsonTweets = []
	
	try:
		# initialize a list to hold all the tweepy Tweets
		tweets = api.user_timeline(screen_name = screen_name, count = tweet_count)
		
		# convert status objects into json
		for tweet in tweets:
			jtweet = tweet._json
			jsonTweets.append(tweet._json)
	except tweepy.error.TweepError:
		print "Cant find page for", screen_name
	return jsonTweets

def delay(minutes, minusSeconds):
	waitTime = 60 * minutes - minusSeconds
	print "delay for", minutes, "minutes minus", minusSeconds, "seconds"

	time.sleep(waitTime)

def saveTweetsForUsers(users, requiredCount, filename):
	getCounter = 0
	startTime = time.time()

	for user in users:
		tweets = getUserTweets(user, requiredCount)
		getCounter += 1
		print "got", len(tweets), "tweets for", user
		print "getCounter:", getCounter

		# write to file
		with open(filename, 'a') as f: # '%s_tweets.txt' % screen_name]
			for tweet in tweets:
				f.write(json.dumps(tweet))
				f.write('\n')
			f.close()

		# check if we need to wait
		if getCounter == 180:
			print "hit rate limit."
			getCounter = 0
			elapsedTime = time.time() - startTime
			delay(15, elapsedTime)

			startTime = time.time()


if __name__ == '__main__':
	#pass in the username of the account you want to download
	users = miningUtils.fileToUserList("streamingResultsFinal.json")
	startIndex = users.index('rinuk86') + 1

	print "starting at index", startIndex
	users = users[startIndex:]
	saveTweetsForUsers(users, 100, "deepSearchedTweetsFinal.txt")

	# for tweet in tweets:
	# 	with open('deepSearchedTweets.txt', 'a') as f: # '%s_tweets.txt' % screen_name
	# 		f.write(json.dumps(tweet))
	print "all done"

