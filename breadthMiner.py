import tweepy
import miningUtils
import json
import time

def loadJsonDicts(filename):
	f = open(filename, 'r')
	lines = f.readlines()
	jsonDicts = []

	for line in lines:
		if not line: continue
		jsonDicts.append(json.loads(line))

	return jsonDicts

def getGameToTitlesMap(gameJson):
	gameToTitles = {}

	for game in gameJson['Games']:
		gameToTitles[game['Title']] = game['Permutations']

	return gameToTitles

def getTitlesQuery(game, gameToTitlesMap):
	query = game

	for title in gameToTitlesMap[game]:
		query += " OR " + title
	# print query
	return query

def saveGameTweets(gameTitle, tweets):
	if len(tweets) == 0: return

	filename = gameTitle.replace(' ', '_') + '_tweets.json'
	f = open('GameTagsMiningResults/' + filename, 'w')
	for tweet in tweets:
		f.write(json.dumps(tweet))
		f.write('\n')
	f.close()

def saveGameTweet(gameTitle, tweet):
	filename = './GameTagsMiningResults/' + gameTitle.replace(' ', '_') + '_tweets.json'
	# print "saved a tweet to" , filename
	print "saving to", filename
	f = open(filename, 'a')
	
	f.write(json.dumps(tweet._json))
	f.write('\n')
	f.close()

def setupAPI():
	consumer_key, consumer_secret, access_key, access_secret = miningUtils.loadKeys("../../keys/keys.txt")

	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_key, access_secret)
	return tweepy.API(auth)

def delay(minutes, minusSeconds):
	waitTime = 60 * minutes - minusSeconds
	print "delay for", minutes, "minutes minus", minusSeconds, "seconds"

	time.sleep(waitTime)

def getSortedGames(gameToTitlesMap):
	games = []

	for game in gameToTitlesMap:
		games.append(game)
	return sorted(games)

gameJson = loadJsonDicts('CurtisFiles/games.json')[0]
gameToTitlesMap = getGameToTitlesMap(gameJson)
sortedGames = getSortedGames(gameToTitlesMap)
api = setupAPI()

startTime = time.time()
hitCount = 0
lastStop = 0
startIndex = 0
print startIndex

for i in range(startIndex, len(sortedGames)):
	game = sortedGames[i]
	print float(i)/float(len(sortedGames)) * 100.0, "percent complete"

	query = getTitlesQuery(game, gameToTitlesMap)
	print "getting results for", query
	cursor = tweepy.Cursor(api.search, q=query).items(limit=536)	
	
	# for tweet in tweepy.Cursor(api.search, q=query).items(3200):
	# 	saveGameTweet(game, tweet)
	while True:
	    try:
	        tweet = cursor.next()
	        saveGameTweet(game, tweet)
	        hitCount += 1
	    except tweepy.TweepError:
	    	print "hit the rate limit after", hitCount - lastStop, "hits"
	        
	        delay(15, 0)
	        startTime = time.time()
	        lastStop = hitCount
	        continue
	    except StopIteration:
	    	print "got", hitCount, "hits before proceeding to next game"
	        break

	print "finished querying for", game

# residueList = [sortedGames[56], sortedGames[57], 'Honkaku_Mahjong:_Tetsuman']
# titleList = ['Pokemon Omega Ruby Alpha Sapphire', 'Pokemon Red Blue', 'Honkaku Mahjong: Tetsuman']
# print residueList

# for i in range(len(residueList)):
# 	game = residueList[i]

# 	query = getTitlesQuery(game, gameToTitlesMap)
# 	print "getting results for", query
# 	cursor = tweepy.Cursor(api.search, q=query).items(limit=536)	
		
# 	# for tweet in tweepy.Cursor(api.search, q=query).items(3200):
# 	# 	saveGameTweet(game, tweet)
# 	while True:
# 	    try:
# 	        tweet = cursor.next()
# 	        title =  titleList[i]
# 	        saveGameTweet(title, tweet)
# 	        hitCount += 1
# 	    except tweepy.TweepError:
# 	    	print "hit the rate limit after", hitCount - lastStop, "hits"
	        
# 	        delay(15, 0)
# 	        startTime = time.time()
# 	        lastStop = hitCount
# 	        continue
# 	    except StopIteration:
# 	    	print "got", hitCount, "hits before proceeding to next game"
# 	        break