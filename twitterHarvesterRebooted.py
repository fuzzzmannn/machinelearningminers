import tweepy
import json
import miningUtils
from tweepy import OAuthHandler

def printKeys(jsonObj):
	keys = jsonObj.keys()

	for i in range(len(keys)):
		print keys[i], "\n"

# def store(tweetList, filename):
# 	f = open(filename, 'w')

# 	for tweet in tweetList:		# write each tweet
# 		f.write(json.dumps(tweet))
# 		f.write("\n")

# 	f.close()

consumer_key, consumer_secret, access_token, access_secret = miningUtils.loadKeys("../keys/keys.txt")

auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
 
api = tweepy.API(auth)
# api.update_status(status='Test from my bot.')

tweetJsonList = []

for status in tweepy.Cursor(api.home_timeline).items(10):
    tweetJsonList.append(status._json)

miningUtils.store(tweetJsonList, "testTweets.txt")