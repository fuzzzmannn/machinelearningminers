import json
import miningUtils
from sets import Set

def removeUsersBelow(userToTweets, threshold):
	pruned = {}

	for user in userToTweets:
		if len(userToTweets[user]) >= threshold:
			pruned[user] = userToTweets[user]
	return pruned

def removeUsersAbove(userToGames, threshold):
	pruned = {}

	for user in userToGames:
		if len(userToGames[user]) <= threshold:
			pruned[user] = userToGames[user]
	return pruned

def getUserToTweets(tweets):
	userToTweets = {}
	skips = 0

	for tweet in tweets:
		if not 'user' in tweet:
			skips += 1
			continue

		user = tweet['user']['screen_name']

		if user in userToTweets:
			userToTweets[user].append(tweet)
		else:
			userToTweets[user] = [tweet]

	print "skipped", skips, "tweets"
	return userToTweets

def getUserToGames(userToTweets, gameToTitlesDict):
	userToGames = {}

	# initialize an empty set for each user
	for user in userToTweets:
		userToGames[user] = Set([])

		for tweet in userToTweets[user]:
			for game in gameToTitlesDict:
				for title in gameToTitlesDict[game]:
					if titleInTweet(tweet, title):
						userToGames[user].add(game)
		if len(userToGames[user]) == 0:
			del userToGames[user]

	return convertDicSetToDicList(userToGames)

def getAverageValsPerKey(dictOfLists):
	valCount = 0.0

	for key in dictOfLists:
		valCount += float(len(dictOfLists[key]))

	return valCount / float(len(dictOfLists))

def getAverageGamesPerUser(userToGames):
	gameCount = 0.0

	for user in userToGames:
		gameCount += float(len(userToGames[user]))

	return gameCount / float(len(userToGames))

def getGameToUsers(tweets, gameToTitlesDict, availableUsers):
	gameToUsers = {}

	# initialize an empty set for each title
	for game in gameToTitlesDict:
		gameToUsers[game] = Set([])

	# get the users who have commented on each game
	for tweet in tweets:		
		for game in gameToTitlesDict:
			for title in gameToTitlesDict[game]:
				if titleInTweet(tweet, title) and tweet['user']['screen_name'] in availableUsers:
					gameToUsers[game].add(tweet['user']['screen_name'])

	return convertDicSetToDicList(gameToUsers)

def titleInTweet(tweet, title):
	try:
		title = title.decode('utf-8')
		text = tweet['text'].decode('utf-8') if 'text' in tweet else ""
	except UnicodeEncodeError:
		# ignore non-English tweet
		return False

	return title.lower() in text.lower() or inHashTags(title, tweet)

def inHashTags(title, tweet):
	hashTags = tweet['entities']['hashtags'] if 'entities' in tweet else []

	for tag in hashTags:
		if title.lower() in tag['text'].lower():
			return True
	return False

def convertDicSetToDicList(dic):
	converted = {}

	for key in dic:
		converted[key] = list(dic[key])

	return converted

def outputJson(filename, dict):
	f = open(filename, 'w')
	f.write(json.dumps(dict))
	f.close()

def printJson(filename): 
	with open(filename, 'r') as f:
		lines = f.readlines() # read only the first tweet/line

		for line in lines:
			tweet = json.loads(line) # load it as Python dict
			print(json.dumps(tweet, indent=4)) # pretty-print

def getUsersChosen(userToGames):
	users = []

	for user in userToGames:
		users.append(user)
	return sorted(users)

def getTweetListForAllGames(gameToTitlesDict):
	sortedGames = getGames(gameToTitlesDict)
	sortedGames.append('Pokemon Omega Ruby Alpha Sapphire')
	allTweets = []

	# print sortedGames
	for i in range(len(sortedGames)):
		gameTitle = sortedGames[i]
		filename = './GameTagsMiningResults/' + gameTitle.replace(' ', '_') + '_tweets.json'
		# print "loading", filename
		try:
			tweets = miningUtils.fileToTweetList(filename)
		except IOError:
			print filename, 'not found'
			continue

		allTweets.extend(tweets)

	# print len(allTweets)
	return allTweets

def getGames(gameToTitlesDict):
	games = []

	for game in gameToTitlesDict:
		games.append(game)
	return sorted(games)

def loadJsonDicts(filename):
	f = open(filename, 'r')
	lines = f.readlines()
	jsonDicts = []

	for line in lines:
		if not line: continue
		jsonDicts.append(json.loads(line))

	return jsonDicts

def getGameToTitlesMap(gameJson):
	gameToTitles = {}

	for i in range (len(gameJson['Games'])):
		game = gameJson['Games'][i]
		titlesList = [game['Title']]
		titlesList.extend(game['Permutations'])
		gameToTitles[titlesList[0]] = titlesList

	return gameToTitles

# tweets = miningUtils.fileToTweetList('streamingResultsFinal.json')
# tweets = miningUtils.fileToTweetList('./GameTagsMiningResults/Smite_tweets.json')
# gameToTitlesDict = miningUtils.loadGameToTitlesDict("gameListFinal.txt")
gameJson = loadJsonDicts('CurtisFiles/games.json')[0]
gameToTitlesDict = getGameToTitlesMap(gameJson)
tweets = getTweetListForAllGames(gameToTitlesDict)
userToTweets = getUserToTweets(tweets)

print "identified", len(userToTweets), "users"
userToGames = getUserToGames(userToTweets, gameToTitlesDict)
print "used", len(userToGames), "users"
print "average", getAverageValsPerKey(userToTweets), "tweets per user"
print "average", getAverageValsPerKey(userToGames), "games per user"

userToGames = removeUsersBelow(userToGames, 2)
print "thinned to", len(userToGames), "users"
userToGames = removeUsersAbove(userToGames, 15)
print "thinned to", len(userToGames), "users"

# outputJson('userToGamesFinal.json', userToGames)
outputJson('userToGamesFinal2.json', userToGames)

gameToUsers = getGameToUsers(tweets, gameToTitlesDict, userToGames)
# outputJson('gameToUsersFinal.json', gameToUsers)
outputJson('gameToUsersFinal2.json', gameToUsers)