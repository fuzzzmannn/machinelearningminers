#!/usr/bin/env python

from bs4 import BeautifulSoup, SoupStrainer
import requests
import string

class Harvester():
	titles = []
	stopPage = 1
	baseURL = 'http://store.steampowered.com/search/#sort_by=Reviews_DESC&page='

	def scrapeTitles(self):
		for page in range(self.stopPage):
			printable = set(string.printable)
			curURL = self.baseURL + str(page + 1)

			r = requests.get(curURL)
			soup = BeautifulSoup(r.content, 'lxml')

			for titleTag in soup.find_all("span", {"class":"title"}):
				title = filter(lambda x: x in printable, titleTag.text)
				self.titles.append(title)

	def printTitleToFile(self, filename):
		f = open(filename, 'w')

		for title in self.titles:
			print title
			f.write(title)
			f.write('\n')
		f.close()

harvey = Harvester()
harvey.scrapeTitles()
harvey.printTitleToFile("steamGames.txt")