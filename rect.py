from PIL import Image

class BoundingRect:
	corners = ()

	def __init__(self, leftX, topY, width, height, image):
		rightX = leftX + width
		bottomY = topY + height

		if rightX == img.size[0]:
			leftX -= width
			rightX -= width
		if bottomY == img.size[1]:
			topY -= height
			bottomY -= height
		#				top-left		bottom-left			top-right		bottom-right
		self.corners = ((leftX, topY), (leftX, bottomY), (rightX, topY), (rightX, bottomY))

	def getPointAt(self, x, y):
	return (self.corners[0][0] + x, self.corners[0][0] + y)

	def getXn(self, rect, n):
		return self.corners[0] + n

	def getYn(self, rect, n):
		return self.corners[1] + n

	def width(self):
		return self.corners[2][0] - self.corners[0][0]

	def height(self):
		return self.corners[1][1] - self.corners[0][1]

	def size(self):
		return (self.width(), self.height())
