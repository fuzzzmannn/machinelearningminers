#!/usr/bin/env python

from bs4 import BeautifulSoup, SoupStrainer
import requests
import threading
import urlparse

whitelist = ["game", "review", "gaming"]

class LinkSeeker(threading.Thread):
	content = []
	links = []
	startUrl = ""

	def __init__(self, startUrl):
		self.startUrl = startUrl
		threading.Thread.__init__(self)

	def run(self):
		r = requests.get(self.startUrl)
		
		soup = BeautifulSoup(r.content, "lxml")
		self.links.extend(self.getPurgedLinkList(self.getAllLinkTags(soup)))

		self.spawnExplorerThreads(self.links)


	def spawnExplorerThreads(self, linkList):
		explorerThreads = []
		print "found", len(linkList), "worthy targets"

		for link in linkList:
			explorerThreads.append(LinkExplorer(link))
			explorerThreads[-1].start()
		for t in explorerThreads:
			t.join()

	def getAllLinkTags(self, soup):
		return soup.findAll('a', href=True)

	def getPurgedLinkList(self, tagList):
		linkList = []

		for tag in tagList:
			url = urlparse.urljoin(self.startUrl, tag['href'])
			
			if containsWhitelisted(url):
				linkList.append(url)

		return linkList

class LinkExplorer(threading.Thread):
	nameToGameDict = {}
	gameToNameDict = {}
	url = ""

	def __init__(self, url):
		self.url = url
		threading.Thread.__init__(self)

	def run(self):
		r = requests.get(self.url)
		soup = BeautifulSoup(r.content, "lxml")

		tweets = soup.find_all("div", {"class":["tweet", "stream-item-header"]})
		print "found", len(tweets), "tweets"
		print tweets[-1].contents
		# for tweet in tweets:
		# 	tweetSoup = soup.findAll("span", {"class":"username"})
		# 	print "i maded the soup"

def containsWhitelisted(url):
	for phrase in whitelist:
		if phrase in url.lower():
			return True
	return False

# class Twitter(threading.Thread):
# 	content = []
	
# 	def __init__(self):
# 		threading.Thread.__init__(self)

# 	def run(self):
# 		r = requests.get("https://twitter.com")
# 		soup = BeautifulSoup(r.content)

# 		self.content = soup.find_all("p", {"class":"tweet-text"})
# 		# print "twitter crap count: ", len(self.content)

# setup threads
threads = []
threads.append(LinkSeeker("https://twitter.com/search?f=users&q=game%20reviews"))

# run threads
for t in threads:
	t.start()
for t in threads:
	t.join()

# # print page content
# print "Content-type: text/html"
# print

# # print headlines
# print "<div style=\"float:left;width:45%\">"
# print "<h1>Headlines</h1>"
# for line in threads[0].content:
# 	print "<li><h3>", line, "</h3></li>"
# print "</div>"

# # print twitter stuff
# print "<div style=\"float:right;width:45%\">"
# print "<h1>Twitter Trends</h1>"
# for line in threads[1].content:
# 	print "<li>", line, "</li>"
# print "</div>"

