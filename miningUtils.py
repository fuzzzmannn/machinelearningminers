import json
from sets import Set

def fileToTweetList(filename):
	f = open(filename, 'r')
	lines = f.readlines()
	tweets = []

	for line in lines:
		if not line: continue
		tweets.append(json.loads(line))

	f.close()
	return tweets

def fileToUserList(filename):
	f = open(filename, 'r')
	lines = f.readlines()
	users = Set([])

	for line in lines:
		if not line: continue
		tweet = json.loads(line)

		if not 'user' in tweet: continue
		users.add(tweet['user']['screen_name'])

	f.close()
	return list(users)

def loadKeys(filename):
	with open(filename) as f:
		consumer_key = f.readline().split('\n')[0]
		consumer_secret = f.readline().split('\n')[0]
		access_token = f.readline().split('\n')[0]
		access_secret = f.readline().split('\n')[0]
	f.close()
	return consumer_key, consumer_secret, access_token, access_secret

def loadTitleList(filename):
	titleList = []

	with open(filename) as f:
		lines = f.readlines()

		for line in lines:
			if not line: continue
			titleTokens = getTitleTokens(line)
			titleList.extend(titleTokens)

	return titleList

def loadGameToTitlesDict(filename):
	titleDict = {}

	with open(filename) as f:
		lines = f.readlines()

		for line in lines:
			if not line: continue

			# get different acceptable titles for same game
			titleTokens = getTitleTokens(line)
			titleDict[titleTokens[0]] = titleTokens

	return titleDict

def getTitleTokens(line):
	return [x.strip().decode('utf-8') for x in line.split(',')]

def store(tweetList, filename):
	f = open(filename, 'w')

	for tweet in tweetList:		# write each tweet
		f.write(json.dumps(tweet))
		f.write("\n")

	f.close()

def unifyGameTitles(sourceFilename, appendageFilename):
	sourceDict = loadGameToTitlesDict(sourceFilename)
	appendageDict = loadGameToTitlesDict(appendageFilename)
	unifiedDict = {}

	for game in sourceDict:
		unifiedDict[game] = [game]

		if game in appendageDict:
			unifiedDict[game] = appendageDict[game]
	return unifiedDict

def buildTitlesString(titleList):
	out = ""

	for i in range(len(titleList)):
		try:
			out += titleList[i]
		except UnicodeEncodeError:
			print "Error with title.  Ignoring"
			continue

		out += "," if i < len(titleList) - 1 else ""
	return out

def dictToNameFile(filename, gameDict):
	f = open(filename, 'w')

	for game in gameDict:
		print game
		gameStr = buildTitlesString(gameDict[game]) 	# peel off the list edges
		f.write(gameStr + "\n")

	f.close()



if __name__ == '__main__':
	gameDict = unifyGameTitles("gameList2.txt", "gameList.txt")
	dictToNameFile("gameListFinal.txt", gameDict)