import json
import miningUtils as utils

def getAllUsers(userToGames):
	userList = []

	for user in userToGames:
		if not user == None:
			userList.append(user)
	return sorted(userList)

def loadAllGames(filename):
	titleDict = utils.loadGameToTitlesDict(filename)
	games = []

	for game in titleDict:
		games.append(game)
	return sorted(games)

def loadJsonDicts(filename):
	f = open(filename, 'r')
	lines = f.readlines()
	jsonDicts = []

	for line in lines:
		if not line: continue
		jsonDicts.append(json.loads(line))

	return jsonDicts

def getUserToGamesString(userToGames, sortedUsers, sortedGames):
	userToGamesVectors = ""
	
	for y in range(len(sortedUsers)):
		user = sortedUsers[y]
		gameString = ""

		for i in range(len(sortedGames)):
			game = sortedGames[i]
			gameString += "1" if game in userToGames[user] else "0"
			gameString += "" if i == len(sortedGames) - 1 else ","

		userToGamesVectors += sortedUsers[y] + " : " + gameString
		userToGamesVectors += "" if y == len(sortedUsers) - 1 else "\n"

	return userToGamesVectors

def getGameToUsersString(gameToUsers, sortedUsers, sortedGames):
	gameToUsersVectors = ""
	
	for y in range(len(sortedGames)):
		game = sortedGames[y]
		print "current game:", game
		userString = ""

		for i in range(len(sortedUsers)):
			# if not game in gameToUsers:
				# print game, "not in gameToUsers"
				# break
			user = sortedUsers[i]
			userString += "1" if user in gameToUsers[game] else "0"
			userString += "" if i == len(sortedUsers) - 1 else ","

		gameToUsersVectors += sortedGames[y] + " : " + userString
		gameToUsersVectors += "" if y == len(sortedGames) - 1 else "\n"

	return gameToUsersVectors

def makeTrainingSets(filename, userToGames, gameToUsers, sortedUsers, sortedGames):
	out = "Users:\n"
	out += listToString(sortedUsers) + "\n\n"
	out += "Games:\n"
	out += listToString(sortedGames) + "\n\n"
	out += "User to Games\n"
	out += getUserToGamesString(userToGames, sortedUsers, sortedGames) + "\n\n"
	out += "Game to Users\n"
	out += getGameToUsersString(gameToUsers, sortedUsers, sortedGames)
	
	f = open(filename, 'w')
	f.write(out)
	f.close()

def listToString(lis):
	listString = ""

	for i in range(len(lis)):
		# print lis[i]
		listString += str(lis[i])
		listString += "" if i == len(lis) - 1 else ","
	return listString

def saveJsonObj(filename, jsonObj):
	f = open(filename, 'w')
	f.write(json.dumps(jsonObj))
	f.close()

# assumes the Games part of gameJson has already been partially populated
def loadTwitterStatsIntoGameJson(gameJson, userToGames, gameToUsers, sortedUsers, sortedGames):
	loadGameToTwitterAuthors(gameJson, gameToUsers, sortedUsers)
	loadTwitterAuthorObjs(gameJson, userToGames, sortedUsers, sortedGames)

def loadTwitterAuthorObjs(gameJson, userToGames, sortedUsers, sortedGames):
	gameJson['TwitterAuthors'] = {}

	for u in range(len(sortedUsers)):
		user = sortedUsers[u]
		authorObj = {}
		
		authorObj['Name'] = user
		authorObj['IndexInList'] = u
		authorObj['Games'] = loadUserToGamesList(user, sortedGames, userToGames)

		gameJson['TwitterAuthors'][user] = authorObj

	print "done loading Twitter author objects"

def loadUserToGamesList(user, sortedGames, userToGames):
	gamePresentList = []

	for game in sortedGames:
		val = 1 if game in userToGames[user] else 0
		gamePresentList.append(val)

	return gamePresentList

def loadGameToTwitterAuthors(gameJson, gameToUsers, sortedUsers):
	for i in range (len(gameJson['Games'])):
		game = gameJson['Games'][i]
		title = game['Title']
		userPresentList = []

		try:
			for user in sortedUsers:
				val = 1 if user in gameToUsers[title] else 0
				userPresentList.append(val)
		except KeyError:
			print title

		game['TwitterAuthors'] = userPresentList

	print "done loading twitter authors"

def getGameToTitlesMap(gameJson):
	gameToTitles = {}

	for i in range (len(gameJson['Games'])):
		game = gameJson['Games'][i]
		titlesList = [game['Title']]
		titlesList.extend(game['Permutations'])
		gameToTitles[titlesList[0]] = titlesList

	return gameToTitles

def getGames(gameToTitlesDict):
	games = []

	for game in gameToTitlesDict:
		games.append(game)
	return sorted(games)

gameJson = loadJsonDicts("CurtisFiles/games.json")[0]
gameToTitlesMap = getGameToTitlesMap(gameJson)
games = getGames(gameToTitlesMap)

userToGames = loadJsonDicts("userToGamesFinal2.json")[0]
gameToUsers = loadJsonDicts("gameToUsersFinal2.json")[0]
users = getAllUsers(userToGames)
# games = loadAllGames("gameListFinal.txt")

gameJson = loadJsonDicts('CurtisFiles/games.json')[0]
loadTwitterStatsIntoGameJson(gameJson, userToGames, gameToUsers, users, games)
saveJsonObj("CurtisFiles/games_modified.json", gameJson)
# makeTrainingSets("trainingSets.txt", userToGames, gameToUsers, users, games)