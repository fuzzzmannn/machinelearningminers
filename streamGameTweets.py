import tweepy
from tweepy import Stream
from tweepy import OAuthHandler
import miningUtils
from tweepy.streaming import StreamListener

class MyListener(StreamListener):
 
    def on_data(self, data):
        try:
            with open('streamingResultsFinal.json', 'a') as f:
                f.write(data)
                return True
        except BaseException as e:
            print("Error on_data: %s" % str(e))
        return True
 
    def on_error(self, status):
        print(status)

        if status == 420:
            return False
        return True

    def on_limit(self, status):
        print 'Limit threshold exceeded', status
        return False

consumer_key, consumer_secret, access_token, access_secret = miningUtils.loadKeys("../keys/keys.txt")

auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
 
api = tweepy.API(auth)
 
tagList = miningUtils.loadTitleList('gameListFinal.txt')

for tag in tagList:
    print tag

twitter_stream = Stream(auth, MyListener())
twitter_stream.filter(track=tagList)